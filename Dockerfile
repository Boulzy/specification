# syntax=docker/dockerfile:1.4

ARG PHP_VERSION=8.2
ARG BASE_IMAGE=base

FROM mlocati/php-extension-installer:2.1 AS php-extension-installer
FROM composer:2.6 AS composer

FROM php:$PHP_VERSION-alpine AS base

WORKDIR /app

ARG USER_NAME=app_user
ENV USER_NAME=app_user
ARG USER_UID=1000
ARG USER_GID=1000

RUN set -eux; \
    addgroup -g $USER_GID $USER_NAME; \
    adduser -D -u $USER_UID -G $USER_NAME -s /bin/sh $USER_NAME; \
    chown -R $USER_UID:$USER_GID /app;

RUN set -eux; \
    apk update && \
    apk add --no-cache \
        bash \
        git \
    ;

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN set -eux; \
    install-php-extensions \
        xdebug \
    ;
ENV XDEBUG_MODE=off

COPY --from=composer --link /usr/bin/composer /usr/local/bin/
ENV COMPOSER_HOME="/home/$USER_NAME/.composer"
ENV PATH="$PATH:$COMPOSER_HOME/vendor/bin"

USER $USER_NAME

COPY --link --chown=$USER_NAME composer.* /app/
RUN if [ -f composer.json ]; then \
        composer install --no-cache --prefer-dist --no-scripts --no-progress; \
    fi

FROM $BASE_IMAGE AS final

COPY --link --chown=$USER_NAME . /app/

USER $USER_NAME
<?php

declare(strict_types=1);

namespace Boulzy\Tests\Specification\Implementation;

final class User
{
    public function __construct(
        private bool $enabled = false,
        private ?Address $address = null,
    ) {
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function enable(): void
    {
        $this->enabled = true;
    }

    public function disable(): void
    {
        $this->enabled = false;
    }

    public function hasAddress(): bool
    {
        return null !== $this->address;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): void
    {
        $this->address = $address;
    }

    public function removeAddress(Address $address): void
    {
        $this->address = null;
    }
}

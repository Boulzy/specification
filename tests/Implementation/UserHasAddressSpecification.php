<?php

declare(strict_types=1);

namespace Boulzy\Tests\Specification\Implementation;

use Boulzy\Specification\CompositeSpecification;

final class UserHasAddressSpecification extends CompositeSpecification
{
    public function isSatisfiedBy($candidate): bool
    {
        if (!$candidate instanceof User) {
            throw new \InvalidArgumentException(\sprintf('%s only supports %s.', self::class, User::class));
        }

        return $candidate->hasAddress();
    }
}

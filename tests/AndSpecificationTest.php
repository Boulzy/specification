<?php

declare(strict_types=1);

namespace Boulzy\Tests\Specification;

use Boulzy\Specification\AndSpecification;
use Boulzy\Specification\Specification;
use Boulzy\Tests\Specification\Implementation\Address;
use Boulzy\Tests\Specification\Implementation\User;
use Boulzy\Tests\Specification\Implementation\UserHasAddressSpecification;
use Boulzy\Tests\Specification\Implementation\UserIsEnabledSpecification;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class AndSpecificationTest extends TestCase
{
    /**
     * @return iterable<mixed[]>
     */
    public static function provider(): iterable
    {
        $a = new UserHasAddressSpecification();
        $b = new UserIsEnabledSpecification();

        $userA = new User(true, new Address('420 Street Fighter', '1955', 'Marty', 'Narnia'));
        $userB = new User();
        $userC = new User(false, new Address('420 Street Fighter', '1955', 'Marty', 'Narnia'));
        $userD = new User(true);

        yield [$a, $b, $userA, true];
        yield [$a, $b, $userB, false];
        yield [$a, $b, $userC, false];
        yield [$a, $b, $userD, false];
    }

    #[DataProvider('provider')]
    public function testIsSatisfiedBy(Specification $a, Specification $b, User $candidate, bool $expected): void
    {
        $this->assertSame($expected, (new AndSpecification($a, $b))->isSatisfiedBy($candidate));
        $this->assertSame($expected, (new AndSpecification($b, $a))->isSatisfiedBy($candidate));
    }
}

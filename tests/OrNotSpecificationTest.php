<?php

declare(strict_types=1);

namespace Boulzy\Tests\Specification;

use Boulzy\Specification\OrNotSpecification;
use Boulzy\Specification\Specification;
use Boulzy\Tests\Specification\Implementation\Address;
use Boulzy\Tests\Specification\Implementation\User;
use Boulzy\Tests\Specification\Implementation\UserHasAddressSpecification;
use Boulzy\Tests\Specification\Implementation\UserIsEnabledSpecification;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class OrNotSpecificationTest extends TestCase
{
    /**
     * @return iterable<mixed[]>
     */
    public static function provider(): iterable
    {
        $a = new UserHasAddressSpecification();
        $b = new UserIsEnabledSpecification();

        $userA = new User(true, new Address('420 Street Fighter', '1955', 'Marty', 'Narnia'));
        $userB = new User();
        $userC = new User(false, new Address('420 Street Fighter', '1955', 'Marty', 'Narnia'));
        $userD = new User(true);

        yield [$a, $b, $userA, true];
        yield [$a, $b, $userB, true];
        yield [$a, $b, $userC, true];
        yield [$a, $b, $userD, false];

        yield [$b, $a, $userA, true];
        yield [$b, $a, $userB, true];
        yield [$b, $a, $userC, false];
        yield [$b, $a, $userD, true];
    }

    #[DataProvider('provider')]
    public function testIsSatisfiedBy(Specification $a, Specification $b, User $candidate, bool $expected): void
    {
        $this->assertSame($expected, (new OrNotSpecification($a, $b))->isSatisfiedBy($candidate));
    }
}

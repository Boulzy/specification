<?php

declare(strict_types=1);

namespace Boulzy\Specification;

/**
 * Class to be extended by specifications that require specification composition.
 */
abstract class CompositeSpecification implements Specification
{
    final public function and(Specification $specification): AndSpecification
    {
        return new AndSpecification($this, $specification);
    }

    final public function andNot(Specification $specification): AndNotSpecification
    {
        return new AndNotSpecification($this, $specification);
    }

    final public function or(Specification $specification): OrSpecification
    {
        return new OrSpecification($this, $specification);
    }

    final public function orNot(Specification $specification): OrNotSpecification
    {
        return new OrNotSpecification($this, $specification);
    }

    final public function not(): NotSpecification
    {
        return new NotSpecification($this);
    }
}

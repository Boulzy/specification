<?php

declare(strict_types=1);

namespace Boulzy\Specification;

/**
 * Specification satisfied when the two conditions are satisfied.
 */
final class AndSpecification extends CompositeSpecification
{
    /**
     * @param Specification $leftCondition  The first condition to satisfy
     * @param Specification $rightCondition The second condition to satisfy
     */
    public function __construct(
        private Specification $leftCondition,
        private Specification $rightCondition,
    ) {
    }

    public function isSatisfiedBy($candidate): bool
    {
        return $this->leftCondition->isSatisfiedBy($candidate) && $this->rightCondition->isSatisfiedBy($candidate);
    }
}

SHELL := bash
ifeq ($(shell uname),Windows_NT)
    SHELL := bash.exe
endif

.SHELLFLAGS := -euo pipefail -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\n\033[1mUsage:\033[0m\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-40s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' Makefile
.DEFAULT_GOAL:=help

##@ [Docker]

DOCKER_IMAGE_NAME=boulzy/$(shell basename "$(CURDIR)")
RUN_ARGS?=
DOCKER_RUN=docker run --rm -it $(RUN_ARGS)

.PHONY: build
BUILD_ARGS?=
build: ## Re-build the containers (args: BUILD_ARGS="--pull --no-cache")
	docker build -t $(DOCKER_IMAGE_NAME) $(BUILD_ARGS) .

.PHONY: sh
sh: ## Open a shell in a container (args: RUN_ARGS="-v ./:/app")
sh:
	@$(DOCKER_RUN) -v ./:/app $(DOCKER_IMAGE_NAME) sh

##@ [CI]

.PHONY: ci
ci: ## Build the image, run the quality analysis tools and run the test suite  (args: RUN_ARGS="-v ./:/app")
ci: build qa test

##@ [Tests]

.PHONY: test
test: ## Run the test suite  (args: RUN_ARGS="-v ./:/app")
test: phpunit #behat

.PHONY: phpunit
phpunit: ## Run PHPUnit tests  (args: RUN_ARGS="-v ./:/app")
	@$(DOCKER_RUN) -e XDEBUG_MODE=coverage $(DOCKER_IMAGE_NAME) vendor/bin/phpunit --coverage-text

#.PHONY: behat
#behat: ## Run Behat tests  (args: RUN_ARGS="-v ./:/app")
#	@$(DOCKER_RUN) vendor/bin/behat

##@ [Quality Analysis]

.PHONY: qa
qa: ## Run the QA tools (args: RUN_ARGS="-v ./:/app")
qa: php-cs-fixer phpstan

.PHONY: php-cs-fixer
php-cs-fixer: ## Run php-cs-fixer (args: RUN_ARGS="-v ./:/app")
	@$(DOCKER_RUN) $(DOCKER_IMAGE_NAME) vendor/bin/php-cs-fixer fix

.PHONY: phpstan
phpstan: ## Run PHPstan analysis (args: RUN_ARGS="-v ./:/app")
	@$(DOCKER_RUN) $(DOCKER_IMAGE_NAME) vendor/bin/phpstan analyze